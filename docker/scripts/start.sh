#!/bin/bash
#
# Setup and run LAVA master
#
echo "INFO: Starting lava master container"

if [ ! -e /etc/lava-dispatcher/certificates.d/master.key ]; then
	echo "INFO: Generate master certificates"
	/usr/share/lava-common/create_certificate.py master
fi

echo "INFO: Starting postgresql"
runuser -u postgres -- /usr/lib/postgresql/9.6/bin/postgres -D /var/lib/postgresql/9.6/main/ \
                                                            -c "config_file=/etc/postgresql/9.6/main/postgresql.conf"&

# Let's give postgres some time to start
sleep 30

echo "INFO: Running web interface"
/usr/bin/gunicorn3 lava_server.wsgi --log-level DEBUG \
                                    --log-file /var/log/lava-server/gunicorn.log \
                                    -u lavaserver \
                                    -g lavaserver \
                                    --workers 1&

a2dissite 000-default
a2enmod proxy
a2enmod proxy_http
a2ensite lava-server.conf
apachectl start

echo "INFO: Starting LAVA master"
/usr/bin/lava-server manage lava-master \
                            --level DEBUG \
                            --master-socket tcp://*:5556 \
                            --encrypt \
                            --master-cert /etc/lava-dispatcher/certificates.d/master.key_secret \
                            --slaves-certs /etc/lava-dispatcher/certificates.d/
